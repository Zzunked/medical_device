import os
from datetime import datetime

import pytest

from lib.config.data import TestData
from lib.main import MedicalDevice


@pytest.fixture
def device_with_default_config():
    return MedicalDevice(
        firmware_revision=TestData.device_default_fw_revision,
        hardware_revision=TestData.device_default_hw_revision
    )


@pytest.hookimpl(tryfirst=True)
def pytest_configure(config):
    if not os.path.exists('report'):
        os.makedirs('report')
    config.option.htmlpath = 'report/'+datetime.now().strftime("%d-%m-%Y %H-%M-%S")+".html"

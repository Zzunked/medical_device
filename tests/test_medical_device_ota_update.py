import pytest

from lib.main import MedicalDevice, Revision
from lib.config.data import TestData


@pytest.mark.parametrize(
    "available_firmware_revision, is_update_required",
    [
        (TestData.major_revision_is_greater_and_minor_is_greater, True),
        (TestData.major_revision_is_greater_and_minor_is_lower, True),
        (TestData.major_revision_is_greater_and_minor_is_equal, True),
        (TestData.major_revision_is_lower_and_minor_is_greater, False),
        (TestData.major_revision_is_lower_and_minor_is_lower, False),
        (TestData.major_revision_is_lower_and_minor_is_equal, False),
        (TestData.major_revision_is_equal_and_minor_is_greater, True),
        (TestData.major_revision_is_equal_and_minor_is_lower, False),
        (TestData.major_revision_is_equal_and_minor_is_equal, False),
    ]
)
def test_firmware_required_checker(
        device_with_default_config: MedicalDevice,
        available_firmware_revision: Revision,
        is_update_required: bool
):
    """Goal: make sure is_new_firmware_required() function works correctly on different set of data"""
    assert device_with_default_config.is_new_firmware_required(available_firmware_revision) == is_update_required


@pytest.mark.parametrize(
    "downloaded_firmware_compatible_hardware_revision, is_hardware_compatible",
    [
        (TestData.major_hw_revision_is_greater_and_minor_is_greater, False),
        (TestData.major_hw_revision_is_greater_and_minor_is_any, False),
        (TestData.major_hw_revision_is_greater_and_minor_is_lower, False),
        (TestData.major_hw_revision_is_greater_and_minor_is_equal, False),
        (TestData.major_hw_revision_is_lower_and_minor_is_greater, False),
        (TestData.major_hw_revision_is_lower_and_minor_is_any, False),
        (TestData.major_hw_revision_is_lower_and_minor_is_lower, False),
        (TestData.major_hw_revision_is_lower_and_minor_is_equal, False),
        (TestData.major_hw_revision_is_equal_and_minor_is_greater, False),
        (TestData.major_hw_revision_is_equal_and_minor_is_any, True),
        (TestData.major_hw_revision_is_equal_and_minor_is_lower, False),
        (TestData.major_hw_revision_is_equal_and_minor_is_equal, True),

    ]
)
def test_hardware_compatibility_checker(
        device_with_default_config: MedicalDevice,
        downloaded_firmware_compatible_hardware_revision: Revision,
        is_hardware_compatible: bool
):
    """Goal: make sure is_firmware_compatible_with_current_hardware()
       function works correctly on different set of data
    """
    assert device_with_default_config.is_firmware_compatible_with_current_hardware(
        downloaded_firmware_compatible_hardware_revision) == is_hardware_compatible


@pytest.mark.parametrize(
    "available_revision, firmware_image, is_update_successful",
    [
        (TestData.available_firmware, TestData.firmware_image, True),
        (TestData.available_firmware_old, TestData.firmware_image, False),
        (TestData.available_firmware_same, TestData.firmware_image, False),

    ]
)
def test_update_firmware_with_different_available_firmware(
        device_with_default_config: MedicalDevice,
        available_revision: Revision,
        firmware_image: bytes,
        is_update_successful: bool
):
    """Goal: make sure device updating only with new available firmware"""
    assert device_with_default_config.update_firmware(
        TestData.cloud_ip_address, available_revision, firmware_image
    ) == is_update_successful


@pytest.mark.parametrize(
    "device_hw_revision, is_update_successful",
    [
        (TestData.major_hw_revision_is_greater_and_minor_is_greater, False),
        (TestData.major_hw_revision_is_greater_and_minor_is_any, False),
        (TestData.major_hw_revision_is_greater_and_minor_is_lower, False),
        (TestData.major_hw_revision_is_greater_and_minor_is_equal, False),
        (TestData.major_hw_revision_is_lower_and_minor_is_greater, False),
        (TestData.major_hw_revision_is_lower_and_minor_is_any, False),
        (TestData.major_hw_revision_is_lower_and_minor_is_lower, False),
        (TestData.major_hw_revision_is_lower_and_minor_is_equal, False),
        (TestData.major_hw_revision_is_equal_and_minor_is_greater, False),
        (TestData.major_hw_revision_is_equal_and_minor_is_lower, False),
    ]
)
def test_update_firmware_with_incompatible_device(device_hw_revision, is_update_successful):
    """Goal: make sure device do not update if hardware revision is not compatible with new firmware update"""
    device = MedicalDevice(
        firmware_revision=TestData.device_default_fw_revision,
        hardware_revision=device_hw_revision
    )
    assert device.update_firmware(
        TestData.cloud_ip_address,
        TestData.available_firmware,
        TestData.firmware_image
    ) == is_update_successful


@pytest.mark.parametrize(
    "firmware_image, is_update_successful",
    [
        (TestData.firmware_image_hw_revision_is_greater_and_minor_is_greater, False),
        (TestData.firmware_image_hw_revision_is_greater_and_minor_is_lower, False),
        (TestData.firmware_image_hw_revision_is_greater_and_minor_is_equal, False),
        (TestData.firmware_image_hw_revision_is_greater_and_minor_is_any, False),
        (TestData.firmware_image_hw_revision_is_lower_and_minor_is_greater, False),
        (TestData.firmware_image_hw_revision_is_lower_and_minor_is_lower, False),
        (TestData.firmware_image_hw_revision_is_lower_and_minor_is_equal, False),
        (TestData.firmware_image_hw_revision_is_lower_and_minor_is_any, False),
        (TestData.firmware_image_hw_revision_is_equal_and_minor_is_greater, False),
        (TestData.firmware_image_hw_revision_is_equal_and_minor_is_lower, False),
        (TestData.firmware_image_hw_revision_is_equal_and_minor_is_equal, True),
        (TestData.firmware_image_hw_revision_is_equal_and_minor_is_any, True),
    ]
)
def test_update_firmware_different_images(
        device_with_default_config: MedicalDevice,
        firmware_image: bytes,
        is_update_successful: bool
):
    """Goal: make sure device check compatibility bytes of new firmware and install only compatible"""
    assert device_with_default_config.update_firmware(
        TestData.cloud_ip_address,
        TestData.available_firmware,
        firmware_image
    ) == is_update_successful

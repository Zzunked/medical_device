#!/bin/bash


set -x

rm -rf .venv
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt

pytest -s -v  --capture=tee-sys --html=report.html

deactivate
rm -rf .venv
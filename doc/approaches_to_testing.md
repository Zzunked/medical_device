# Approaches to testing medical device OTA firmware update
There is different approaches to testing on different levels

## Automated tests
### Unit tests
It is very important to write unit tests for each function, to be sure they are working as expected independently on
different data sets. To check functions behave correctly we can use pairwise method to collect data and parametrize
tests 


### API testing
On next step it is important to write api tests, so we can check how device communicate with Cloud in different
situations. Here we can test positive cases when firmware downloaded successful and different negative cases when
Cloud unreachable, network is broken etc. To perform such tests we have to mock Cloud, so we can generate different
Cloud behaviour in tests.


### Integration tests and E2E testing
During integration test we will check how our device communicate with real Cloud and make sure that our device
compatible with real Cloud and will work correctly in production. To perform such automated test we have to build
environment with device software and cloud software.


## Manual testing
It is good practice to perform smoke/sanity tests with real device in lab environment. Manual tests can show problems
with user experience and device maintenance also highlight real environment issues.


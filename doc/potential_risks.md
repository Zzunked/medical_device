## Questions for clarification and potential risks
* Why hardware compatibility is checked only after firmware download?
Looks like we could check hardware compatibility before downloading.
* How can we check that downloaded firmware bin file is not broken/corrupted? Can we retry download?
* Can we roll back to previous firmware if new one installed incorrect?
* Should we keep this firmware.bin file after installation or remove it?
* Should we check how much memory do we have before firmware downloading?
* What will happen if new firmware way new (couple of major revision) than already installed? Is there any required
backward compatibility?
* In case of failed installation of compatible firmware should we retry installation or wait for next update on next
day?
* Can we have pool of Cloud IP addresses so if one of them is not available, device could try another one?
* Should we check if downloaded firmware same version as expected?
* What max and min allowed size of firmware image?
* Should device check if other processes in progress (sample checking for example), so firmware update wont affect
users?

# Medical Device OTA firmware update requirements

## Connect to the Portal
* Device should establish connection with Portal
* connect() function should return true if connection with portal established successful and false otherwise
* Device should log if connection established successful
* Retry if first connection attempt failed
* Reconnect if connection was dropped

## Check available firmware
* Device should check the latest firmware revision available from Portal
* Device should get both major and minor revision of available firmware
* If there is no available firmware device should log warning and stop updating process
* Retry request if Portal respond with error or request timeout expired

## Check if new firmware is required
* Device should check current installed firmware major and minor revision
* Device should compare current firmware revision and revision available from Cloud
* New firmware is required if:
  * Major revision of available firmware is greater than major revision already installed firmware
  * Major revision of available firmware is equal to already installed firmware, but minor revision is greater than
  already installed
* New firmware is NOT required if:
  * Major revision of available firmware is lower than already installed
  * Major revision of available firmware is equal to already installed but minor revision is lower than already
  installed
  * Both major and minor revisions of available firmware are equal to already installed 
* If new firmware is not required device should stop updating process and try it next day


## Download firmware from Portal
* Device should download new firmware if it is required
* Result of downloading is a binary file (array of bytes)
* Retry request on errors and timeouts
* Check firmware binary is not corrupt


## Check firmware and hardware compatibility
* Device should check current hardware major and minor revision
* Device should check if hardware is compatible with new firmware
* Hardware is compatible if:
  * Major and minor compatible hardware revisions of the firmware is the same as major and minor revisions of the
  device hardware
  * Major compatible hardware revision of firmware is th same as current device major hardware revision and
  minor compatible hardware revision of firmware is 0xFFFF (compatible with any minor hardware revision)
* Hardware is NOT compatible if:
  * Major compatible hardware revision of firmware do not match with device major hardware revision
  * Major compatible hardware revision of firmware is same as current device major hardware revision BUT
  minor hardware revision of firmware (not 0xFFFF) does not match with minor hardware revision
* If new firmware is no compatible with current device hardware, device should stop updating process and try it next day


## Program new firmware
* Device should install new firmware
* program_firmware() function returns True in case of successful installation and False otherwise 
* If installation was not successful, device should log warning message and stop update process


## Reboot to finish firmware installation
* Device should finish firmware installation with reboot 

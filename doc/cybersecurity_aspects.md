# Cybersecurity aspects and improvements
* Connection to Portal should be established over TLS latest version (1.3) protocol
* Do not use outdated ciphers (latest TSL 1.3 do not use them already)
* Drop connection after download is done
* Do not allow other devices connect to our medical device
* Do not use unprotected public wifi (for example in hospitals)
* Always install security patches for device operating system
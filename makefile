SHELL := /bin/bash


.PHONY: prepare_env
prepare_env:
	rm -rf .venv; \
	python3 -m venv .venv; \
	. .venv/bin/activate; \
	pip install -r requirements.txt; \
	deactivate

.PHONY: run_p0_p1_tests
run_tests: prepare_env
	. .venv/bin/activate; \
	pytest -s -v --capture=tee-sys
#!/bin/bash


set -x


docker pull python:3.8-slim-buster
docker run -it -v "$(pwd)":/medical_device -w /medical_device python:3.8-slim-buster /bin/bash run_tests.sh
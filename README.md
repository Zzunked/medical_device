# Medical device OTA update prototype
## Description
Test assignment mini-project from Osler-Diagnostic for Software Test Engineer position
___
## Navigation:
### lib folder 
* Device OTA update script prototype in main.py
* Test data in config/data.py
### tests folder
* Tests for script prototype
### doc folder
* Requirements for the OTA update functionality in the device in ota_update_requirements.md
* List of potential approaches to testing in approaches_to_testing.md
* List of potential risks in potential_risks.md
* Cybersecurity aspects in cybersecurity_aspects.md
## Usage
### Prerequisites
### To run local

- python3, python3-pip

### To run in docker

- docker installed

### Run local

To run all tests execute:
```bash
sh run_tests.sh
```

### Run in Docker

To run all tests in docker execute:
```bash
sh run_tests_in_docker.sh
```

### Run in gitlab-ci

Trigger pipeline for main branch

### Access to test reports 

You can see test reports in job's artifacts. Open them in your browser.

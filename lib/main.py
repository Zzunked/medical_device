import logging
from dataclasses import dataclass
from typing import Tuple


@dataclass
class Revision:
    major_revision: int
    minor_revision: int


class Firmware:
    def __init__(
            self,
            major_revision: int,
            minor_revision: int,
            major_hw_compatible_revision: int,
            minor_hw_compatible_revision: int,
            image: bytes,
            crc_bit: bytes,
            full_image: bytes
    ):
        self.major_revision = major_revision
        self.minor_revision = minor_revision
        self.major_hw_compatible_revision = major_hw_compatible_revision
        self.minor_hw_compatible_revision = minor_hw_compatible_revision
        self.image = image
        self.crc_bit = crc_bit
        self.full_image = full_image

    @property
    def revision(self) -> Revision:
        return Revision(self.major_revision, self.minor_revision)

    @property
    def compatible_hw_revision(self) -> Revision:
        return Revision(self.major_hw_compatible_revision, self.minor_hw_compatible_revision)


class MedicalDevice:
    def __init__(self, firmware_revision: Revision, hardware_revision: Revision):
        self.current_device_firmware_revision = firmware_revision
        self.current_device_hardware_revision = hardware_revision

    @staticmethod
    def connect(ip_address: str, connected: bool = True) -> bool:
        if connected:
            logging.info(f"Connected to portal: {ip_address}")
        else:
            logging.warning(f"Failed to connect to portal {ip_address}")
        return connected

    @staticmethod
    def retrieve_firmware_available(available_revision: Revision, got_version: bool = True) -> Tuple[int, int]:
        if not got_version:
            logging.warning("Failed to retrieve new firmware version!")
        else:
            logging.info("Retrieved firmware version successful!")
            return available_revision.major_revision, available_revision.minor_revision

    def get_device_firmware_revision(self) -> Revision:
        logging.info(
            "Current device firmware version: "
            f"{self.current_device_firmware_revision.major_revision}."
            f"{self.current_device_firmware_revision.minor_revision}"
        )
        return self.current_device_firmware_revision

    @staticmethod
    def download_firmware(firmware_image: bytes, downloaded: bool = True) -> bytes:
        if not downloaded:
            logging.warning("Failed to download firmware!")
        else:
            logging.info("Firmware downloaded successful!")
            return firmware_image

    def get_device_hardware_revision(self) -> Revision:
        logging.info(
            "Current device hardware version: "
            f"{self.current_device_hardware_revision.major_revision}."
            f"{self.current_device_hardware_revision.minor_revision}"
        )
        return self.current_device_hardware_revision

    def program_firmware(self, firmware_image: bytes, installed_successful: bool = True) -> bool:
        fw_revision, _ = self.parse_firmware_image_info(firmware_image)
        if not installed_successful:
            logging.warning(
                f"Failed to install firmware version:{fw_revision.major_revision}.{fw_revision.minor_revision}!"
            )
        else:
            logging.info(
                f"Firmware installed successful version: {fw_revision.major_revision}.{fw_revision.minor_revision}"
            )
        return installed_successful

    @staticmethod
    def reboot():
        logging.info("Reboot device")

    def is_new_firmware_required(self, available_firmware: Revision) -> bool:
        if available_firmware.major_revision > self.current_device_firmware_revision.major_revision:
            return True
        elif available_firmware.major_revision == self.current_device_firmware_revision.major_revision:
            if available_firmware.minor_revision > self.current_device_firmware_revision.minor_revision:
                return True
            else:
                return False
        else:
            return False

    @staticmethod
    def parse_firmware_image_info(firmware_image: bytes) -> Tuple[Revision, Revision]:
        firmware = Firmware(
            major_revision=int(firmware_image[0:1].hex(), 16),
            minor_revision=int(firmware_image[1:2].hex(), 16),
            major_hw_compatible_revision=int(firmware_image[2:4].hex(), 16),
            minor_hw_compatible_revision=int(firmware_image[4:6].hex(), 16),
            image=firmware_image[7:-1],
            crc_bit=firmware_image[-1:],
            full_image=firmware_image
        )
        return firmware.revision, firmware.compatible_hw_revision

    def is_firmware_compatible_with_current_hardware(self, compatible_hw_revision: Revision) -> bool:
        any_version = 0xFFFF
        if self.current_device_hardware_revision.major_revision == compatible_hw_revision.major_revision:
            if compatible_hw_revision.minor_revision == any_version:
                return True
            elif compatible_hw_revision.minor_revision == self.current_device_hardware_revision.minor_revision:
                return True
            else:
                return False
        else:
            return False

    def update_firmware(
            self,
            portal_ip_address: str,
            available_revision: Revision,
            firmware_image: bytes,
            connection_successful: bool = True
    ) -> bool:
        connected = self.connect(portal_ip_address, connected=connection_successful)
        if connected:
            available_firmware_major_revision, available_firmware_minor_revision = self.retrieve_firmware_available(
                available_revision
            )
            available_firmware = Revision(available_firmware_major_revision, available_firmware_minor_revision)
            if self.is_new_firmware_required(available_firmware):
                downloaded_firmware_image = self.download_firmware(firmware_image)
                downloaded_fw_revision, downloaded_compatible_hw_revision = self.parse_firmware_image_info(
                    downloaded_firmware_image
                )
                if self.is_firmware_compatible_with_current_hardware(downloaded_compatible_hw_revision):
                    program_result = self.program_firmware(downloaded_firmware_image)
                    if program_result:
                        self.reboot()
                        return True
                    else:
                        logging.warning(
                            "Failed to install firmware version: "
                            f"{downloaded_fw_revision.major_revision}.{downloaded_fw_revision.minor_revision}"
                        )
                        return False
                else:
                    logging.warning("Downloaded firmware is not compatible with device hardware version!")
                    return False
            else:
                logging.warning(
                    "Device firmware is up to date:\n"
                    "Device firmware version:"
                    f"{self.current_device_firmware_revision.major_revision}."
                    f"{self.current_device_firmware_revision.minor_revision}\n"
                    f"Available from cloud firmware version:"
                    f"{available_firmware.major_revision}.{available_firmware.minor_revision}"
                )
                return False
        else:
            logging.warning("Can not establish connection with Cloud")
            return False

from dataclasses import dataclass
from random import randint
from typing import Tuple

from lib.main import Revision

ANY_MINOR_HW_REVISION: int = 0xFFFF
DEVICE_DEFAULT_MAJOR_FW_REVISION: int = 2
DEVICE_DEFAULT_MINOR_FW_REVISION: int = 4
DEVICE_DEFAULT_MAJOR_HW_REVISION: int = 513
DEVICE_DEFAULT_MINOR_HW_REVISION: int = 8721
AVAILABLE_DEFAULT_MAJOR_FW_REVISION: int = 2
AVAILABLE_DEFAULT_MINOR_FW_REVISION: int = 5


def gen_firmware_image(
        fw_revision: Revision,
        compatible_hw_revision: Revision,
        firmware_image_len: int = 65
) -> bytes:
    def split_hw_revision_in_two_bytes(revision: int) -> Tuple[int, int]:
        first_byte = int(hex(revision)[2:].zfill(4)[0:2], 16)
        second_byte = int(hex(revision)[2:].zfill(4)[2:], 16)
        return first_byte, second_byte

    first_byte_compatible_hw_major_revision, second_byte_compatible_hw_major_revision = split_hw_revision_in_two_bytes(
        compatible_hw_revision.major_revision
    )
    first_byte_compatible_hw_minor_revision, second_byte_compatible_hw_minor_revision = split_hw_revision_in_two_bytes(
        compatible_hw_revision.minor_revision
    )
    revision_info = [
        fw_revision.major_revision,
        fw_revision.minor_revision,
        *[
            first_byte_compatible_hw_major_revision,
            second_byte_compatible_hw_major_revision
        ],
        *[
            first_byte_compatible_hw_minor_revision,
            second_byte_compatible_hw_minor_revision
        ]
    ]
    rest_image = [randint(0, 255) for _ in range(firmware_image_len)]
    return bytes(revision_info + rest_image)


def get_firmware_image() -> bytes:
    with open("lib/config/firmware.bin", "rb") as firmware:
        return firmware.read()


class TestData:
    firmware_compatible_device_hardware_revision = Revision(
        DEVICE_DEFAULT_MAJOR_HW_REVISION, DEVICE_DEFAULT_MINOR_HW_REVISION
    )
    cloud_ip_address = "192.168.0.1"
    firmware_image = get_firmware_image()

    # Firmware revision
    device_default_fw_revision = Revision(DEVICE_DEFAULT_MAJOR_FW_REVISION, DEVICE_DEFAULT_MINOR_FW_REVISION)
    major_revision_is_greater_and_minor_is_greater = Revision(
        DEVICE_DEFAULT_MAJOR_FW_REVISION + 1, DEVICE_DEFAULT_MINOR_FW_REVISION + 1
    )
    major_revision_is_greater_and_minor_is_lower = Revision(
        DEVICE_DEFAULT_MAJOR_FW_REVISION + 1, DEVICE_DEFAULT_MINOR_FW_REVISION - 1
    )
    major_revision_is_greater_and_minor_is_equal = Revision(
        DEVICE_DEFAULT_MAJOR_FW_REVISION + 1, DEVICE_DEFAULT_MINOR_FW_REVISION
    )
    major_revision_is_lower_and_minor_is_greater = Revision(
        DEVICE_DEFAULT_MAJOR_FW_REVISION - 1, DEVICE_DEFAULT_MINOR_FW_REVISION + 1
    )
    major_revision_is_lower_and_minor_is_lower = Revision(
        DEVICE_DEFAULT_MAJOR_FW_REVISION - 1, DEVICE_DEFAULT_MINOR_FW_REVISION - 1
    )
    major_revision_is_lower_and_minor_is_equal = Revision(
        DEVICE_DEFAULT_MAJOR_FW_REVISION - 1, DEVICE_DEFAULT_MINOR_FW_REVISION
    )
    major_revision_is_equal_and_minor_is_greater = Revision(
        DEVICE_DEFAULT_MAJOR_FW_REVISION, DEVICE_DEFAULT_MINOR_FW_REVISION + 1
    )
    major_revision_is_equal_and_minor_is_equal = Revision(
        DEVICE_DEFAULT_MAJOR_FW_REVISION, DEVICE_DEFAULT_MINOR_FW_REVISION
    )
    major_revision_is_equal_and_minor_is_lower = Revision(
        DEVICE_DEFAULT_MAJOR_FW_REVISION, DEVICE_DEFAULT_MINOR_FW_REVISION - 1
    )

    # Hardware revision
    device_default_hw_revision = Revision(DEVICE_DEFAULT_MAJOR_HW_REVISION, DEVICE_DEFAULT_MINOR_HW_REVISION)
    major_hw_revision_is_greater_and_minor_is_greater = Revision(
        DEVICE_DEFAULT_MAJOR_HW_REVISION + 1, DEVICE_DEFAULT_MINOR_HW_REVISION + 1
    )
    major_hw_revision_is_greater_and_minor_is_any = Revision(
        DEVICE_DEFAULT_MAJOR_HW_REVISION + 1, ANY_MINOR_HW_REVISION
    )
    major_hw_revision_is_greater_and_minor_is_lower = Revision(
        DEVICE_DEFAULT_MAJOR_HW_REVISION + 1, DEVICE_DEFAULT_MINOR_HW_REVISION - 1
    )
    major_hw_revision_is_greater_and_minor_is_equal = Revision(
        DEVICE_DEFAULT_MAJOR_HW_REVISION + 1, DEVICE_DEFAULT_MINOR_HW_REVISION
    )
    major_hw_revision_is_lower_and_minor_is_greater = Revision(
        DEVICE_DEFAULT_MAJOR_HW_REVISION - 1, DEVICE_DEFAULT_MINOR_HW_REVISION + 1
    )
    major_hw_revision_is_lower_and_minor_is_any = Revision(
        DEVICE_DEFAULT_MAJOR_HW_REVISION - 1, ANY_MINOR_HW_REVISION
    )
    major_hw_revision_is_lower_and_minor_is_lower = Revision(
        DEVICE_DEFAULT_MAJOR_HW_REVISION - 1, DEVICE_DEFAULT_MINOR_HW_REVISION - 1
    )
    major_hw_revision_is_lower_and_minor_is_equal = Revision(
        DEVICE_DEFAULT_MAJOR_HW_REVISION - 1, DEVICE_DEFAULT_MINOR_HW_REVISION
    )
    major_hw_revision_is_equal_and_minor_is_greater = Revision(
        DEVICE_DEFAULT_MAJOR_HW_REVISION, DEVICE_DEFAULT_MINOR_HW_REVISION + 1
    )
    major_hw_revision_is_equal_and_minor_is_any = Revision(
        DEVICE_DEFAULT_MAJOR_HW_REVISION, ANY_MINOR_HW_REVISION
    )
    major_hw_revision_is_equal_and_minor_is_lower = Revision(
        DEVICE_DEFAULT_MAJOR_HW_REVISION, DEVICE_DEFAULT_MINOR_HW_REVISION - 1
    )
    major_hw_revision_is_equal_and_minor_is_equal = Revision(
        DEVICE_DEFAULT_MAJOR_HW_REVISION, DEVICE_DEFAULT_MINOR_HW_REVISION
    )

    # Available firmware
    available_firmware = Revision(AVAILABLE_DEFAULT_MAJOR_FW_REVISION, AVAILABLE_DEFAULT_MINOR_FW_REVISION)
    available_firmware_old = Revision(DEVICE_DEFAULT_MAJOR_FW_REVISION - 1, DEVICE_DEFAULT_MINOR_FW_REVISION)
    available_firmware_same = Revision(DEVICE_DEFAULT_MAJOR_FW_REVISION, DEVICE_DEFAULT_MINOR_FW_REVISION)

    # Generated firmware images with different hw compatibility and same available firmware revision
    firmware_image_hw_revision_is_greater_and_minor_is_greater = gen_firmware_image(
        fw_revision=available_firmware,
        compatible_hw_revision=major_hw_revision_is_greater_and_minor_is_greater
    )
    firmware_image_hw_revision_is_greater_and_minor_is_lower = gen_firmware_image(
        fw_revision=available_firmware,
        compatible_hw_revision=major_hw_revision_is_greater_and_minor_is_lower
    )
    firmware_image_hw_revision_is_greater_and_minor_is_equal = gen_firmware_image(
        fw_revision=available_firmware,
        compatible_hw_revision=major_hw_revision_is_greater_and_minor_is_equal
    )
    firmware_image_hw_revision_is_greater_and_minor_is_any = gen_firmware_image(
        fw_revision=available_firmware,
        compatible_hw_revision=major_hw_revision_is_greater_and_minor_is_any
    )
    firmware_image_hw_revision_is_lower_and_minor_is_greater = gen_firmware_image(
        fw_revision=available_firmware,
        compatible_hw_revision=major_hw_revision_is_lower_and_minor_is_greater
    )
    firmware_image_hw_revision_is_lower_and_minor_is_lower = gen_firmware_image(
        fw_revision=available_firmware,
        compatible_hw_revision=major_hw_revision_is_lower_and_minor_is_lower
    )
    firmware_image_hw_revision_is_lower_and_minor_is_equal = gen_firmware_image(
        fw_revision=available_firmware,
        compatible_hw_revision=major_hw_revision_is_lower_and_minor_is_equal
    )
    firmware_image_hw_revision_is_lower_and_minor_is_any = gen_firmware_image(
        fw_revision=available_firmware,
        compatible_hw_revision=major_hw_revision_is_lower_and_minor_is_any
    )
    firmware_image_hw_revision_is_equal_and_minor_is_greater = gen_firmware_image(
        fw_revision=available_firmware,
        compatible_hw_revision=major_hw_revision_is_equal_and_minor_is_greater
    )
    firmware_image_hw_revision_is_equal_and_minor_is_lower = gen_firmware_image(
        fw_revision=available_firmware,
        compatible_hw_revision=major_hw_revision_is_equal_and_minor_is_lower
    )
    firmware_image_hw_revision_is_equal_and_minor_is_equal = gen_firmware_image(
        fw_revision=available_firmware,
        compatible_hw_revision=major_hw_revision_is_equal_and_minor_is_equal
    )
    firmware_image_hw_revision_is_equal_and_minor_is_any = gen_firmware_image(
        fw_revision=available_firmware,
        compatible_hw_revision=major_hw_revision_is_equal_and_minor_is_any
    )
